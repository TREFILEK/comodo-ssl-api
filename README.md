# README #

A NodeJS library for accessing the Comodo SSL Web Services API.

Currently implements:
getCustomerCertTypesByOrg
getCustomerCertTypes
enroll
updateRequesterExt
getCollectStatus
collect

Needs to implement Revoke/Renew.
